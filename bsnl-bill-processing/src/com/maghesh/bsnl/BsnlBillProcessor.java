package com.maghesh.bsnl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.File;
import java.io.FileNotFoundException;

public class BsnlBillProcessor {

	// private static final String FILENAME = "F:\\abc-colors\\test.txt";
	private static String postScriptFile = "";
	static StringBuilder contents = new StringBuilder();

	static StringBuilder csvContent = new StringBuilder();
	//static int prePrintCount = 0;
	//static int plainPrintCount = 0;
	static String inputPostScriptFolderName = "";
	static String currentFileName = "";
	static int fileCount = 0;
	
	//static int duplexCount = 0;
	//static int simplexCount = 0;
	//static int previousBillPageCounter = 0;
	
	//static int intCurrentBillPageCount = 0;

	//static int new_billCount = 0;
	//static int new_plainCount = 0;
	//static int new_billPrintCount = 0;
	//static int new_totalCount = 0;
	static int new_currentBillTotalPages = 0; // for entire total pages in current bill (remember one PS file can have more than one bills)
	static int new_currentBillBillCount = 0; // for counting the total number if bills in current bill (remember one PS file can have more than one bills)
	static int new_currentBillPlainCount = 0; // for counting total number of plain pages current bill (remember one PS file can have more than one bills)
	static int new_currentPsFileBillCount = 0; // for counting the bills in the entire current PS file (one PS file)
	static int new_currentPsFilePlainCount = 0; // for counting the plainpages in the entire current PS file (one PS file)

	static int new_currentBillSingleSidePrintCount = 0; // for counting single side of one single bill
	static int new_currentBillDoubleSidePrintCount = 0; // for counting double side pages of one single bill

	static int new_currentPsFileSingleSidePrintCount = 0; // for counting single side of one PS bill
	static int new_currentPsFileDoubleSidePrintCount = 0; // for counting double side pages of one PS bill
	


	
	// last modifed on 25-july-2019

	public static void main(String[] args) throws FileNotFoundException, IOException, URISyntaxException {

		long startTime = System.nanoTime();

		if (args.length == 0) {
			System.out.println("Please specify the folder that contains the PS file(s)");
			System.exit(0);
		}

		addCSVHeader();
		
		inputPostScriptFolderName = args[0];
		//inputPostScriptFolderName = "D:\\temp\\akshara-bsnl-java-program\\temp";
		readContents(inputPostScriptFolderName);

		long endTime = System.nanoTime();
		long duration = (endTime - startTime) / 1000000000;

		writeCsvContentToFile();
		System.out.println("Completed " + fileCount + " file(s) in " + duration + " second(s)");
	}






	static public void readContents(String inputPostScriptFolderName)
			throws FileNotFoundException, IOException, URISyntaxException {
		File file = new File(inputPostScriptFolderName);
		File[] files = file.listFiles();
		for (File f : files) {
			System.gc();
			
			ResetPrintCounters(); 

			contents.setLength(0); // reset the file content

			if (f.isFile() && getExtensionOfFile(f).equals("ps")) { // if it is a file with extension ".ps"
				
				fileCount = fileCount + 1;

				System.out.print("Started processing " + f.getName() + "..........");

				postScriptFile = inputPostScriptFolderName + "\\" + f.getName();
				currentFileName = f.getName(); // this variable is used for csv reporting

				BufferedReader br = null;
				FileReader fr = null;

				try {

					// br = new BufferedReader(new FileReader(FILENAME));
					fr = new FileReader(postScriptFile);
					br = new BufferedReader(fr);

					String sCurrentLine;
					String currentBillPageCount = "";
					boolean duplexStringReplaced = false;
					while ((sCurrentLine = br.readLine()) != null) {
						
					
						if(sCurrentLine.contains("(Page")) {
							Pattern pattern = Pattern.compile("(\\(Page 1 of)(.*?)(\\))"); // find the string "(Page 1 of 4)" in the PS file
							Matcher matcher = pattern.matcher(sCurrentLine);
							if (matcher.find())
							{
								
								new_currentBillBillCount = 1; // counts the number of bills in current bill, which will always be 1. this variable is actually not required. just introduced for clear variable naming convention
							//	new_billCount = new_billCount + 1;
								new_currentPsFileBillCount = new_currentPsFileBillCount + new_currentBillBillCount; // counting the total number of bills for the entire PS file

								System.out.println("\n" + matcher.group());
								//System.out.println(matcher.group(2));
								new_currentBillTotalPages = Integer.parseInt(matcher.group(2).trim());
								System.out.println("current bill page count = " + new_currentBillTotalPages); // remember, a PS file can have more than one bills

								//new_currentBillTotalPages = 1;

							//	new_plainCount = new_plainCount + new_currentBillTotalPages - 2;
								new_currentBillPlainCount = new_currentBillTotalPages - 2; // deducting the front page count (that is 2)
								
								//new_currentBillDoubleSidePrintCount = new_currentBillPlainCount / 2;  // finding the double side pages excluding the front page, of one single bill
								//new_currentBillDoubleSidePrintCount = (new_currentBillTotalPages - new_currentBillPlainCount)/2;  // finding the double side pages excluding the front page, of one single bill
								new_currentBillDoubleSidePrintCount = ((new_currentBillTotalPages - 2)/2);  // finding the double side pages excluding the front page, of one single bill
								
								
								//new_currentBillSingleSidePrintCount = new_currentBillPlainCount - (new_currentBillDoubleSidePrintCount * 2); //fnding the single side pages of one single bill
								//new_currentBillSingleSidePrintCount = new_currentBillTotalPages - (new_currentBillDoubleSidePrintCount * 2); //fnding the single side pages of one single bill
								new_currentBillSingleSidePrintCount = ((new_currentBillTotalPages - 2) % 2); //fnding the single side pages of one single bill
								
								new_currentPsFilePlainCount = new_currentPsFilePlainCount + new_currentBillPlainCount;

								new_currentPsFileDoubleSidePrintCount = new_currentPsFileDoubleSidePrintCount + new_currentBillDoubleSidePrintCount;
								new_currentPsFileSingleSidePrintCount = new_currentPsFileSingleSidePrintCount + new_currentBillSingleSidePrintCount;
								//System.out.println("current bill plain count = " + (new_currentBillTotalPages - 2));
								System.out.println("current bill bill count = " + new_currentBillBillCount);
								System.out.println("current bill plain count = " + (new_currentBillPlainCount));
								/*if(new_currentBillTotalPages % 2 == 0) {
									System.out.println("this bill has two pages only");
								}
								else {
									System.out.println("this bill has more than two pages");
									System.out.println("the extra page count is : " + new_currentBillTotalPages % 2);
								}*/



							}							
						}
						
						
						/*if (sCurrentLine.contains("%%Page:")) {
							//System.out.println(sCurrentLine);

							currentBillPageCount = sCurrentLine.split(" ")[1];
							//System.out.println("=======>currentBillPageCount " + currentBillPageCount);
						
							// the below lines are to calculate the count of DUPLEX pages and SIMPLEX pages after printing
							//COUNTING CODE STARTS ===========================================================================
							intCurrentBillPageCount = Integer.valueOf(currentBillPageCount); //converting it as integer and storing for easiness
							if(Integer.valueOf(currentBillPageCount) > 1) {  // if the current bill has more than one pages
								
								if((intCurrentBillPageCount - 1) == previousBillPageCounter) { // if it is the continuous page of the current bill
									if(intCurrentBillPageCount % 2 == 0) { // if current bill page count % 2 is zero
										simplexCount = simplexCount + 1;  // then it shall be a single page print
									}
									else {
										duplexCount = duplexCount + 1; // else it shall be a duplex page print (prints on both sides)
										simplexCount = simplexCount - 1; // since it is printing on both sides...we need to deduct one from simplex count.
									}
								}

							}
							
							previousBillPageCounter = intCurrentBillPageCount;
							//=============================================================================== END - COUNTING CODE

						} */

						if (duplexStringReplaced == false) {
							contents.append(sCurrentLine);
							contents.append(System.getProperty("line.separator"));
						} else if(!sCurrentLine.contains("<</Duplex")) {
							contents.append(sCurrentLine);
							contents.append(System.getProperty("line.separator"));							
						}
						else {
							duplexStringReplaced = false;
						}

						if (sCurrentLine.contains("%%EndPageSetup")) {
							if (currentBillPageCount.equals("1")) {
								contents.append(
										"<</Duplex false/Tumble false /PageSize [595 842] /MediaPosition 1 /ImagingBBox null >> setpagedevice");
								contents.append(System.getProperty("line.separator"));
								//prePrintCount = prePrintCount + 1;
							} else if(currentBillPageCount.equals("2")){
								contents.append(
										"<</Duplex true/Tumble false /PageSize [595 842] /MediaPosition 2 /ImagingBBox null >> setpagedevice");
								contents.append(System.getProperty("line.separator"));
								duplexStringReplaced = true; // a variable to keep track... so that the actual 'Duplex'
																// which is there in the file already can be avoided
								//plainPrintCount = plainPrintCount + 1;
							}
							else {
								//plainPrintCount = plainPrintCount + 1; // to keep track of count of print on back side of paper also (duplex printing)
							}

						}
						

					} // end of file lines parsing

					//System.out.println("\nnew bill count = " + new_billCount);
					//System.out.println("new plain count = " + new_plainCount);

					System.out.println("\nnew bill count = " + new_currentPsFileBillCount);
					System.out.println("new plain count = " + new_currentPsFilePlainCount);

				} catch (IOException e) {

					e.printStackTrace();

				} finally {

					try {

						if (br != null)
							br.close();

						if (fr != null)
							fr.close();

					} catch (IOException ex) {

						ex.printStackTrace();

					}

				}

				String newFilename = inputPostScriptFolderName + "\\CORRECTED\\" + f.getName();
				String newFoldername = inputPostScriptFolderName + "\\CORRECTED\\";
				writeContents(newFilename, contents);
				
				addCSVContent(); // write the csv file to disk
				
				System.out.println("finished successfully.");
			}

			
			
			
		}
	}



	static public void writeContents(String newFileName, StringBuilder contents)
			throws FileNotFoundException, IOException, URISyntaxException {

		try {

			File yourFile = new File(newFileName);
			yourFile.getParentFile().mkdirs();
			yourFile.delete();
			//yourFile.createNewFile(); // if file already exists will do nothing

			FileWriter writer = new FileWriter(yourFile, true);
			BufferedWriter bufferedWriter = new BufferedWriter(writer);
			bufferedWriter.write(contents.toString());
			bufferedWriter.newLine();

			bufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		finally {
			// bufferedWriter.close();
		}
	}
	
	static void addCSVHeader() {
		//csvContent.append("FolderName,FileName,Preprinted,Plain,TotalPage, ,Duplex,Simplex");
		csvContent.append("FolderName,FileName,BillCount,Plain,BillPrintCount,TotalCount,SingleSide,DoubleSide");
		csvContent.append(System.getProperty("line.separator"));
	}
	
	private static void ResetPrintCounters() {
		// TODO Auto-generated method stub
	//	prePrintCount = 0;
	//	plainPrintCount = 0;
	//	
	//	simplexCount = 0;
	//	duplexCount = 0;
		
	//	previousBillPageCounter = 0;


		new_currentBillTotalPages = 0; 
		new_currentBillBillCount = 0; 
		new_currentBillPlainCount = 0; 
		new_currentPsFileBillCount = 0;
		new_currentPsFilePlainCount = 0;


		new_currentBillSingleSidePrintCount = 0; 
		new_currentBillDoubleSidePrintCount = 0; 
	
		new_currentPsFileSingleSidePrintCount = 0; 
		new_currentPsFileDoubleSidePrintCount = 0; 		
		


	}
	
	static void addCSVContent() {
		//int totalPages = prePrintCount + plainPrintCount;
		//csvContent.append( inputPostScriptFolderName + "," + currentFileName + "," + prePrintCount + "," + plainPrintCount + "," +totalPages + ",," + duplexCount + "," + simplexCount );
		csvContent.append( inputPostScriptFolderName + "," + currentFileName + "," + new_currentPsFileBillCount + "," + new_currentPsFilePlainCount + "," +new_currentPsFileBillCount * 2 + "," + ((new_currentPsFileBillCount * 2)+ new_currentPsFilePlainCount) + "," + new_currentPsFileSingleSidePrintCount + "," + new_currentPsFileDoubleSidePrintCount );
		csvContent.append(System.getProperty("line.separator"));
	}
	
	private static void writeCsvContentToFile() throws IOException {
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		
		File csvFile = new File(inputPostScriptFolderName + "\\EXCEL-REPORT-" + timeStamp + ".csv");
		csvFile.createNewFile();
		FileWriter writer = new FileWriter(csvFile, true);
		BufferedWriter bufferedWriter = new BufferedWriter(writer);
		bufferedWriter.write(csvContent.toString());
		bufferedWriter.close();
		
		
	}
	
	public static String getExtensionOfFile(File file)
	{
		String fileExtension="";
		// Get file Name first
		String fileName=file.getName();
		
		// If fileName do not contain "." or starts with "." then it is not a valid file
		if(fileName.contains(".") && fileName.lastIndexOf(".")!= 0)
		{
			fileExtension=fileName.substring(fileName.lastIndexOf(".")+1);
		}
		
		return fileExtension;
	}


}
